import { useState } from 'react';


function useTodoState() {
  const [todos, setTodos] = useState([]);

  const saveTodo = (todo) => {
    setTodos([...todos, {
      id: todos.length + 1,
      done: false,
      value: todo
    }]);
  }

  const updateTodoList = (todos) => setTodos([...todos])

  return [todos, saveTodo, updateTodoList];

}

export default useTodoState;

