import React, { useState } from 'react';

import AddTodoForm from './components/addTodo';
import Filters from './components/filters';
import TodoList from './components/todoList';

import useTodoState from './hooks/useTodoState'

import './App.css';


function App() {
  const [todos, saveTodo, updateTodoList] = useTodoState();
  const [activeFilter, setActiveFilter] = useState(null)


  const todoListItems = activeFilter !== null
    ? todos.filter(todo => todo.done === activeFilter)
    : todos;

  return (
    <div className="app">
      <header>
        <h2>Todo App with Hooks</h2>
      </header>
      <main>
        <AddTodoForm saveTodo={saveTodo} />
        <Filters active={activeFilter} onChange={setActiveFilter}/>
        <TodoList todos={todoListItems} updateTodoList={updateTodoList} />
      </main>
    </div>
  )
}

export default App;
