import React from 'react';
import { ListGroupItem } from 'reactstrap';

function TodoItem({todo, onStatusChange}) {
  return (
    <ListGroupItem href="#"
      key={todo.id}
      style={{ textDecoration: todo.done ? 'line-through' : '' }}
      onClick={onStatusChange(todo)}>
      {todo.value}
    </ListGroupItem>
  )
}

export default TodoItem;