import React from 'react';
import { Button, ButtonGroup } from 'reactstrap';


function Filters({ onChange }) {
  return (
    <div className="filters">
      <ButtonGroup>
        <Button color="link" onClick={() => onChange(null)}>All</Button>
        <Button color="link" onClick={() => onChange(true)}>
          Compleated
        </Button>
        <Button color="link" onClick={() => onChange(false)}>
          Undone
        </Button>
      </ButtonGroup>
    </div>
  )
}

export default Filters;