
import React, { useState } from 'react';
import { Form, Input } from 'reactstrap';

const addTodoPlaceholderText = "what do you want to do..."

function AddTodoForm({saveTodo, placeholderText}) {
  const [newTodoValue, setNewTodoValue] = useState("");
  const onInputChange = (e) => setNewTodoValue(e.target.value);

  const addTodo = (e) => {
    e.preventDefault();

    saveTodo(newTodoValue);
    setNewTodoValue('');
  }

  return (
    <Form onSubmit={addTodo}>
      <Input
        type="text"
        value={newTodoValue}
        onChange={onInputChange}
        placeholder = {placeholderText || addTodoPlaceholderText}
      />
    </Form>
  )
}

export default AddTodoForm;