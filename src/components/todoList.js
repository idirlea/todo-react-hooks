import React from 'react';

import TodoItem from './todoItem';
import { ListGroup, Alert } from 'reactstrap';

function TodoList({ todos, updateTodoList }) {
  const onStatusChange = (todoItem) => () => {
    const index = todos.findIndex(todo => todo.id === todoItem.id);
    todos[index] = { ...todoItem, done: !todoItem.done };

    updateTodoList(todos);
  }

  if (todos.length === 0) {
    return (
      <Alert color="info">
        You have nothing to do :)
      </Alert>
    )
  }

  return (
    <ListGroup>
      {todos.map(todo => (
        <TodoItem key={todo.id} todo={todo} onStatusChange={onStatusChange} />
      ))}
    </ListGroup>
  )
}

export default TodoList;